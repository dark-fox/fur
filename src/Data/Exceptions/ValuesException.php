<?php
namespace DarkFox\Fur\Data\Exceptions;

use Exception;

class ValuesException extends Exception
{
  public const DF_VALUES_EXCEPTION_NULL_AS_STRING = 1;
  public const DF_VALUES_JSON_ERROR = 2;

}
