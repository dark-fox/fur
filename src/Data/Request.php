<?php
namespace DarkFox\Fur\Data;

use DarkFox\Fur\Core\Run;

class Request
{
  private ?Values $Values = null;

  public function __construct() {
    if (is_null($this->Values)) {
      $this->Values = new Values;
    }
  }

  /**
   * Return value from $_SERVER variable if field exits.
   *
   * @param string $field Field to get.
   * @return Values
   */
  public function server(string $field): Values {
    if (isset($_SERVER[$field])) {
      $this->Values->setValue($_SERVER[$field]);
    }

    return $this->Values;
  }

  /**
   * Return value from $_SERVER['REQUEST_URI'] in Framework format.
   *
   * @return array
   * @throws Exceptions\ValuesException
   */
  public function query(): array {
    $query = $this->server('REQUEST_URI')->string(false);
    $path = explode('/', $query);

    array_shift($path);

    if (isset($path[0]) && '' === $path[0]) {
      array_shift($path);
    }

    if (0 === count($path)) {
      $path[] = isset($path[0]) && '' !== $path[0] ? ucfirst($path[0]) : Run::DEFAULT_MODULE;
      $path[] = isset($path[1]) && '' !== $path[1] ? ucfirst($path[1]) : Run::DEFAULT_ACTION;
    }

    return $path;
  }

  /**
   * Return value from $_REQUEST variable if field exits.
   *
   * @param string $field Field to get.
   * @return Values
   */
  public function request(string $field): Values {
    $value = null;

    if (isset($_REQUEST[$field])) {
      $value = $_REQUEST[$field];
    }

    return $this->Values->setValue($value);
  }

  /**
   * Return value from $_COOKIE if cookie exists.
   *
   * @param string $name Cookie name to get.
   * @return Values
   */
  public function cookie(string $name): Values {
    $value = null;

    if (isset($_COOKIE[$name])) {
      $value = $_COOKIE[$name];
    }

    return $this->Values->setValue($value);
  }

  /**
   * Return value from $_GET if variable exists.
   *
   * @param string $variable Variable to get.
   * @return Values
   */
  public function get(string $variable): Values {
    $value = null;

    if (isset($_GET[$variable])) {
      $value = $_GET[$variable];
    }

    return $this->Values->setValue($value);
  }

  /**
   * Return value from $_POST if variable exists.
   *
   * @param string $variable Variable to get.
   * @return Values
   */
  public function post(string $variable): Values {
    $value = null;

    if (isset($_POST[$variable])) {
      $value = $_POST[$variable];
    }

    return $this->Values->setValue($value);
  }

  /**
   * Return value from $_FILES if variable exists.
   *
   * @param string $variable Variable to get.
   * @return Values
   */
  public function file(string $variable): Values {
    $value = null;

    if (isset($_FILES[$variable])) {
      $value = $_FILES[$variable];
    }

    return $this->Values->setValue($value);
  }

  /**
   * Return value from $_SESSION variable if field exits.
   *
   * @param string $field Field to get.
   * @return Values
   */
  public function session(string $field): Values {
    $value = null;

    if (isset($_SESSION[$field])) {
      $value = $_SESSION[$field];
    }

    return $this->Values->setValue($value);
  }

}
