<?php
namespace DarkFox\Fur\Data;

use DarkFox\Fur\Data\Exceptions\ValuesException;
use Exception;

class Values
{
  /** @var mixed */
  private $value;

  public function __construct($value = null) {
    $this->setValue($value);
  }

  /**
   * Set value for further proceed.
   *
   * @param mixed $value
   * @return Values
   */
  public function setValue( $value ): Values {
    $this->value = $value;
    return $this;
  }

  /**
   * Return given value as integer.
   *
   * @return int
   */
  public function int(): int {
    return (int)$this->value;
  }

  /**
   * Return given value as float.
   *
   * @return float
   */
  public function float(): float{
    return (float)$this->value;
  }

  /**
   * Return given value as string.
   *
   * @param bool $strict Determine if we allow to process nullable values as string.
   * @return string|null
   * @throws ValuesException
   */
  public function string(bool $strict = true): ?string {
    if ( $strict && is_null($this->value) ) {
      throw new ValuesException(
        'Given values is null. You can\'t cast null to string.',
        ValuesException::DF_VALUES_EXCEPTION_NULL_AS_STRING,
      );
    }

    return (string)$this->value;
  }

  /**
   * Return given value as clean string (with htmlentities()).
   *
   * @return string
   */
  public function clearString(): string {
    try {
      $value = $this->string(true);
    } catch ( Exception $exception ) {
      $value = '';
    }

    return (string) htmlentities($value);
  }

  /**
   * Return given value as boolean.
   *
   * @param bool $strict When value is string, we check if contains 't' or 'true' to return true - in any other cases - we return false.
   * @return bool
   */
  public function bool(bool $strict = true): bool {
    if ($strict && is_string($this->value)) {
      $testValue = mb_strtolower($this->clearString());

      switch ($testValue) {
        case 't':
        case 'true':
          return true;
        default:
          return false;
      }
    } else {
      return (bool)$this->value;
    }
  }

  /**
   * Return given value as array. Usable when we received json string.
   *
   * @return array
   * @throws ValuesException
   */
  public function json(): array {
    $json = [];

    if (is_string($this->value)) {
      $json = json_decode($this->value, true);

      if (is_null($json)) {
        throw new ValuesException(
          sprintf('Error on decoding value to an array: %s', json_last_error_msg()),
          ValuesException::DF_VALUES_JSON_ERROR,
        );
      }
    }

    return $json;
  }

  /**
   * Return raw, unparsed value.
   * @return mixed
   */
  public function raw() {
    return $this->value;
  }

}
