<?php
namespace DarkFox\Fur\Core;

use DarkFox\Fur\Action\Action;
use DarkFox\Fur\Action\CliAction;
use DarkFox\Fur\Action\ViewAction;
use DarkFox\Fur\Core\Exceptions\RunException;
use DarkFox\Fur\Data\Exceptions\ValuesException;
use DarkFox\Fur\Data\Request;
use Error;
use Exception;

final class Run
{
  public const DEFAULT_MODULE = 'Index';
  public const DEFAULT_ACTION = 'IndexAction';

  private const CONTROLLER_SUFFIX = 'Action';
  private const HTTP_CONTROLLERS_DIR = 'Actions';
  private const CLI_CONTROLLERS_DIR = 'CLI';

  private ?Request $Request;
  private string $controller = '';
  private string $namespace = '';
  private string $environment = '';
  private array $path = [
    'module' => '',
    'action' => '',
  ];

  public function __construct(string $namespace = null) {
    $this->namespace = $namespace ?? '';
    $this->Request = new Request;

    $this->setPath()->setEnvironment()->handleExceptions()->run();
  }

  /**
   * Determine PHP environment.
   *
   * @return Run
   */
  private function setEnvironment(): Run {
    $this->environment = php_sapi_name();
    return $this;
  }

  /**
   * Handle exceptions.
   *
   * @return Run
   */
  private function handleExceptions(): Run {
    set_exception_handler(function ($exception) {
      $Handler = ExceptionConfig::getInstance()->handler;

      if ($exception instanceof Exception) {
        if (class_exists($Handler)) {
          $Handler = new $Handler;

          if (method_exists($Handler, 'handle')) {
            $Handler->handle($exception);
          } else {
            ExceptionHandler::printOutput($exception);
          }
        } else {
          ExceptionHandler::printOutput($exception);
        }
      } else {
        ExceptionHandler::printOutput($exception);
      }
    });

    return $this;
  }

  /**
   * Proceed base flow: check environment and create correct path to Controller.
   * At the end - run it.
   *
   * @throws RunException
   */
  private function run(): void {
    $this->buildPath()->runController();
  }

  /**
   * Run controller class if exits.
   *
   * @throws RunException
   */
  private function runController(): void {
    $this->controller;

    if (!class_exists($this->controller)) {
      if (0 !== substr_compare($this->controller, self::CONTROLLER_SUFFIX, -strlen(self::CONTROLLER_SUFFIX))) {
        $this->controller .= self::CONTROLLER_SUFFIX;
      }

      if (!class_exists($this->controller)) {
        throw new RunException(
          sprintf('Class "%s" not found.', $this->controller),
          RunException::DF_ACTION_NOT_FOUND,
        );
      }
    }

    $action = new $this->controller;

    if ($action instanceof Action) {
      if ($this->isCLI() && $action instanceof ViewAction) {
        throw new RunException(
          sprintf('Action "%s" is created for View environment. Cant\'t be run as CLI Action.', $this->controller),
          RunException::DF_WRONG_ENVIRONMENT,
        );
      }

      if (!$this->isCLI() && $action instanceof CliAction) {
        throw new RunException(
          sprintf('Action "%s" is created for CLI environment. Cant\'t be run as View Action.', $this->controller),
          RunException::DF_WRONG_ENVIRONMENT,
        );
      }

      if (method_exists($action, 'run')) {
        $action->run();
      } else {
        throw new RunException(
          sprintf('Action "%s" does not have "run" method that is needed for processing.', $this->controller),
          RunException::DF_NO_RUN_METHOD,
        );
      }
    } else {
      throw new RunException(
        sprintf('Class "%s" is not Action instance.', $this->controller),
        RunException::DF_CLASS_IS_NOT_ACTION,
      );
    }
  }

  /**
   * Create path based on provided URL.
   *
   * @return Run
   */
  private function setPath(): Run {
    try {
      $request = $this->Request->query();
      $this->path = [
        'module' => isset($request[0]) && is_string($request[0]) && '' !== $request[0] ? $request[0] : static::DEFAULT_MODULE,
        'action' => isset($request[1]) && is_string($request[1]) && '' !== $request[1] ? $request[1] : static::DEFAULT_ACTION,
      ];
    } catch (ValuesException $e) {
      ExceptionHandler::printOutput($e);
    }

    return $this;
  }

  /**
   * Build path to controller.
   *
   * @return Run
   */
  private function buildPath(): Run {
    $this->controller = join('\\', [
      $this->namespace,
      $this->path['module'],
      $this->isCLI() ? static::CLI_CONTROLLERS_DIR : static::HTTP_CONTROLLERS_DIR,
      $this->path['action'],
    ]);

    return $this;
  }

  /**
   * Check if environment is CLI.
   *
   * @return bool
   */
  private function isCLI(): bool {
    return 'cli' === $this->environment;
  }

}