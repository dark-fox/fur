<?php
namespace DarkFox\Fur\Core;

use Error;
use Exception;

class ExceptionHandler
{
  /**
   * Display Exception in correct format.
   *
   * @param Exception|Error $exception Exception instance to display.
   * @param bool            $doDie     Kill PHP process when set to true.
   */
  public static function printOutput($exception, bool $doDie = true): void {
    $exceptionPath = explode('\\', get_class($exception));

    echo sprintf('<h1>%s</h1>', end($exceptionPath));

    echo sprintf('<code>Code: %s</code><br>', $exception->getCode());
    echo $exception->getMessage();

    echo '<br><br>';

    echo sprintf('<code>File: %s</code><br>', $exception->getFile());
    echo sprintf('<code>Line: %s</code><br>', $exception->getLine());

    echo sprintf('<pre>%s</pre>', $exception->getTraceAsString());

    if ($doDie) {
      die;
    }
  }

}
