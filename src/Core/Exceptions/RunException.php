<?php
namespace DarkFox\Fur\Core\Exceptions;

use Exception;

class RunException extends Exception
{
  public const DF_ACTION_NOT_FOUND = 1;
  public const DF_CLASS_IS_NOT_ACTION = 2;
  public const DF_WRONG_ENVIRONMENT = 3;
  public const DF_NO_RUN_METHOD = 4;

}
