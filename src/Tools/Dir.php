<?php
namespace DarkFox\Fur\Tools;

use DarkFox\Fur\Tools\Exceptions\ToolsException;
use ReflectionClass;
use ReflectionException;

class Dir
{
  public const COMPOSER_DIR = 'vendor';
  public const PROJECT_DIR = 'src';

  private const NAMESPACE_SEPARATOR = '\\';

  /**
   * Return Class absolute path.
   *
   * @param object $className Class to test.
   * @return string
   * @throws ToolsException|ReflectionException
   */
  public static function getAbsolute(object $className): string {
    $reflection = new ReflectionClass($className);
    $classPath = $reflection->getFileName();
    $position = mb_strpos($classPath, static::COMPOSER_DIR);

    if (false === $position) {
      $position = mb_strpos($classPath, static::PROJECT_DIR);
    }

    if (false === $position) {
      throw new ToolsException(sprintf('Directories %s or %s must appear in path.', static::COMPOSER_DIR, static::PROJECT_DIR), ToolsException::DF_DIR_NO_DIRECTORIES);
    }

    return mb_substr($classPath, 0, $position);
  }

  /**
   * Return Class directory.
   *
   * @param object $className Class to test.
   * @return string
   * @throws ReflectionException
   */
  public static function getClassDir(object $className): string {
    $reflection = new ReflectionClass($className);
    return dirname($reflection->getFileName());
  }

  /**
   * Get real path to given Class.
   *
   * @param object $className Class to test.
   * @param string $separator Separator to cut string.
   * @return string
   * @throws ToolsException
   */
  public static function getRealPath(object $className, string $separator = self::NAMESPACE_SEPARATOR): string {
    $className = get_class($className);
    $classNamespaces = explode(self::NAMESPACE_SEPARATOR, $className);

    if (isset($classNamespaces[0])) {
      $classNamespaces[0] = static::PROJECT_DIR;
      $className = join($separator, $classNamespaces);

      return $className;
    } else {
      throw new ToolsException(sprintf('Given class name "%s" has no namespace.', $className), ToolsException::DF_DIR_CLASS_HAS_NO_NAMESPACE);
    }
  }

}
