<?php
namespace DarkFox\Fur\Tools;

abstract class Singleton
{
  protected static array $instance = [];

  /**
   * @return $this
   */
  public static function getInstance() {
    $calledClass = get_called_class();

    if (!isset(static::$instance[$calledClass])) {
      static::$instance[$calledClass] = new static;
    }

    return static::$instance[$calledClass];
  }

  private function __construct() {}
  private function __clone() {}

}
