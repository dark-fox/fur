<?php
namespace DarkFox\Fur\Tools;

use DarkFox\Fur\Data\Exceptions\ValuesException;
use DarkFox\Fur\Data\Request;

class Url
{
  public const WITH_CURRENT_URL = 0;
  public const USE_URL_BASE = 1;
  public const CLEAN_URL = 2;

  /**
   * Returns current URL.
   *
   * @return string
   * @throws ValuesException
   */
  public static function getCurrentUrl(): string {
    return sprintf('/%s/', join('/', (new Request)->query()));
  }

  /**
   * Build URL out of given parameters.
   *
   * @param array $parameters Parameters to add to URL.
   * @param int   $flag       Add current URL to beginning.
   * @return string
   * @throws ValuesException
   */
  public static function buildUrl(array $parameters = [], int $flag = self::CLEAN_URL): string {
    $url = join('/', $parameters);

    switch ($flag) {
      case static::WITH_CURRENT_URL:
        $url = sprintf('%s%s', static::getCurrentUrl(), $url);
        break;
      case static::USE_URL_BASE:
        $query = (new Request)->query();

        if (2 === count($query)) {
          $url = sprintf('/%s/%s', join('/', [$query[0], $query[1]]), $url);
        }

        break;
      default:
        $url = sprintf('/%s/', $url );
        break;
    }

    return $url;
  }

}
