<?php
namespace DarkFox\Fur\Tools;

use DarkFox\Fur\Tools\Exceptions\ToolsException;

class Factory
{
  protected array $implementations = [];
  private ?string $implementation;

  /**
   * Return list of possible implementations.
   *
   * @return array
   */
  public function getImplementations(): array {
    return $this->implementations;
  }

  /**
   * Select one of the implementations and assigns it to the class variable.
   *
   * @param string $implementation Implementation name.
   * @return Factory
   * @throws ToolsException
   */
  public function setImplementation(string $implementation): Factory {
    if (array_key_exists($implementation, $this->implementations)) {
      $this->implementation = $this->implementations[$implementation];
    } elseif (in_array($implementation, $this->implementations, true)) {
      $this->implementation = $implementation;
    } else {
      throw new ToolsException(
        sprintf('Implementation "%s" does not exits in implementations array.', $implementation),
        ToolsException::DF_FACTORY_IMPLEMENTATION_DOES_NOT_EXITS,
      );
    }

    return $this;
  }

  /**
   * Create new instance of class stored in implementation variable.
   *
   * @param mixed ...$args
   * @return mixed
   * @throws ToolsException
   */
  public function create(... $args) {
    if (is_string($this->implementation) && class_exists($this->implementation)) {
      if (is_subclass_of($this->implementation, Singleton::class)) {
        return ($this->implementation)::getInstance();
      } else {
        return new $this->implementation(... $args);
      }
    } else {
      throw new ToolsException(
        sprintf('Implemented class "%s" does not exits.', $this->implementation),
        ToolsException::DF_FACTORY_IMPLEMENTED_CLASS_DOES_NOT_EXITS,
      );
    }
  }

}
