<?php
namespace DarkFox\Fur\Tools\Exceptions;

use Exception;

class ToolsException extends Exception
{
  public const DF_DIR_NO_DIRECTORIES = 100;
  public const DF_DIR_CLASS_HAS_NO_NAMESPACE = 101;
  public const DF_FACTORY_IMPLEMENTATION_DOES_NOT_EXITS = 200;
  public const DF_FACTORY_IMPLEMENTED_CLASS_DOES_NOT_EXITS = 201;

}
