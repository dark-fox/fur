<?php
namespace DarkFox\Fur\Tools;

use DarkFox\Fur\Data\Request;
use DarkFox\Fur\Data\Values;

class Cookie
{
  public const COOKIE_EXPIRATION_TIME = 7 * 60 * 60 * 24; //7 days

  protected ?Request $Request = null;

  public function __construct() {
    if (is_null($this->Request)) {
      $this->Request = new Request;
    }
  }

  /**
   * Get cookie with given name.
   *
   * @param string $cookieName Name of cookie to get.
   * @return Values
   */
  public function get(string $cookieName): Values {
    return $this->Request->cookie($cookieName);
  }

  /**
   * Wrapper for setcookie() function. Set cookie with default values.
   *
   * @param string   $cookieName Name of cookie to set.
   * @param mixed    $value      Value to set.
   * @param int|null $expiration Cookie expiration time. Default is 7 days.
   * @param string   $path       Cookie path.
   * @param string   $domain     Cookie domain
   * @param bool     $secure     Should cookie be transmitted HTTPS only? Default: false.
   * @param bool     $httpOnly   Should cookie be transmitted HTTP only? Default: false.
   * @return Cookie
   */
  public function set(string $cookieName, $value, int $expiration = null, string $path = '', string $domain = '', bool $secure = false, bool $httpOnly = false): Cookie {
    if ($expiration === null) {
      $expiration = time() + static::COOKIE_EXPIRATION_TIME;
    }

    setcookie($cookieName, $value, $expiration, $path, $domain, $secure, $httpOnly);
    return $this;
  }

  /**
   * Set cookie and immediately returns it's value.
   *
   * @param string   $cookieName Name of cookie to set.
   * @param mixed    $value      Value to set.
   * @param int|null $expiration Cookie expiration time. Default is 7 days.
   * @param string   $path       Cookie path.
   * @param string   $domain     Cookie domain
   * @param bool     $secure     Should cookie be transmitted HTTPS only? Default: false.
   * @param bool     $httpOnly   Should cookie be transmitted HTTP only? Default: false.
   * @return Values
   */
  public function setAndGet(string $cookieName, $value, int $expiration = self::COOKIE_EXPIRATION_TIME, string $path = '', string $domain = '', bool $secure = false, bool $httpOnly = false): Values {
    return $this->set($cookieName, $value, $expiration, $path, $domain, $secure, $httpOnly)->get($cookieName);
  }

  /**
   * Verify if cookie exists.
   *
   * @param string $cookieName Name of cookie to test.
   * @return bool
   */
  public function exists(string $cookieName): bool {
    return isset($_COOKIE[$cookieName]);
  }

  /**
   * Removes cookie.
   *
   * @param string $cookieName Name of cookie to delete.
   * @return $this
   */
  public function destroy(string $cookieName): Cookie {
    $this->set($cookieName, '', time() - 3600);
    return $this;
  }

}
