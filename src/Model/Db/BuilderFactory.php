<?php
namespace DarkFox\Fur\Model\Db;

use DarkFox\Fur\Model\Db\Builder\MySqlBuilder;
use DarkFox\Fur\Model\Db\Builder\PgSqlBuilder;
use DarkFox\Fur\Tools\Factory;

class BuilderFactory extends Factory
{
  protected array $implementations = [
    MySqlBuilder::class,
    PgSqlBuilder::class,
  ];

}
