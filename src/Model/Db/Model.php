<?php
namespace DarkFox\Fur\Model\Db;

use DarkFox\Fur\Core\ExceptionHandler;
use DarkFox\Fur\Model\Db\Builder\Builder;
use DarkFox\Fur\Model\Db\Builder\IBuilder;
use DarkFox\Fur\Model\Db\Builder\MySqlBuilder;
use DarkFox\Fur\Model\Db\Builder\PgSqlBuilder;
use DarkFox\Fur\Model\Db\Exceptions\DbModelException;
use Exception;
use PDOStatement;

abstract class Model
{
  protected ?string $tableName;

  private Connector $Connector;
  private IBuilder $Builder;

  public function __construct() {
    $this->Connector = Connector::getInstance();

    try {
      $this->Connector->connect();
      $this->createBuilder()->verifyTableName();
      $this->Builder->setTableName($this->tableName);
    } catch (Exception $e) {
      ExceptionHandler::printOutput($e);
    }
  }

  /**
   * Mark query as SELECT and provide elements to select to the Builder.
   *
   * @param array $what Elements to select from database. All will be returned as default.
   * @return $this
   */
  public function select(array $what = []): Model {
    $this->Builder->createSelect($what);
    return $this;
  }

  /**
   * Mark query as INSERT and provide elements to add to query by the Builder.
   *
   * @param array $what Elements to store in database.
   * @return $this
   */
  public function insert(array $what = []): Model {
    $this->Builder->createInsert($what);
    return $this;
  }

  /**
   * Mark query as UPDATE and provide elements to add to query by the Builder.
   *
   * @param array $what Elements to store in database.
   * @return $this
   */
  public function update(array $what = []): Model {
    $this->Builder->createUpdate($what);
    return $this;
  }

  /**
   * Mark query as COUNT and provide elements to add to query by the Builder.
   *
   * @return $this
   */
  public function count(): Model {
    $this->Builder->createCount();
    return $this;
  }

  /**
   * Mark query as DELETE and provide elements to add to query by the Builder.
   *
   * @return $this
   */
  public function delete(): Model {
    $this->Builder->createDelete();
    return $this;
  }

  /**
   * Mark query as CUSTOM. Do not over use this method while it allows to write custom queries.
   *
   * @param string $query Query to process.
   * @return $this
   */
  public function custom(string $query = ''): Model {
    $this->Builder->createCustom($query);
    return $this;
  }

  /**
   * Process WHERE part to the query.
   *
   * @param string $where WHERE query.
   * @return $this
   */
  public function where(string $where): Model {
    $this->Builder->createWhere($where);
    return $this;
  }

  /**
   * Set limit for the query. If set to 0, limit will be disabled.
   *
   * @param int $limit Results limiter.
   * @return $this
   */
  public function limit(int $limit = Builder::DEFAULT_LIMIT): Model {
    if (0 >= $limit) {
      $this->Builder->disableLimit();
    } else {
      $this->Builder->enableLimit()->createLimit($limit);
    }

    return $this;
  }

  /**
   * Set offset for the query.
   *
   * @param int $offset Offset to set.
   * @return $this
   */
  public function offset(int $offset = Builder::DEFAULT_OFFSET): Model {
    if (0 >= $offset) {
      $offset = 0;
    }

    $this->Builder->createOffset($offset);

    return $this;
  }

  /**
   * Add possibility to order fields.
   *
   * @param array $fields Fields to order by. [field => asc/desc]
   * @return $this
   */
  public function order(array $fields): Model {
    $this->Builder->createOrder($fields);
    return $this;
  }

  /**
   * Add possibility to group fields.
   *
   * @param array $fields Fields to order by. [field => asc/desc]
   * @return $this
   */
  public function group(array $fields): Model {
    $this->Builder->createGroup($fields);
    return $this;
  }

  /**
   * Add possibility to join other tables.
   *
   * @param string $table     Joined table name.
   * @param string $joinField Field name from other table.
   * @param string $ownField  Own field name.
   * @param string $type      Type of join, left by default.
   * @return $this
   */
  public function join(string $table, string $joinField, string $ownField, string $type = 'left'): Model {
    $this->Builder->createJoin($table, $joinField, $ownField, $type);
    return $this;
  }

  /**
   * Process PDO's fetch() method.
   *
   * @return array
   */
  public function fetch(): array {
    $Statement = $this->prepare();
    $results = [];

     if ($this->Connector->isExecuted()) {
       $results = $Statement->fetch();

       if (!$results) {
         $results = [];
       }
     }

    return $results;
  }

  /**
   * Process PDO's fetchAll() method.
   *
   * @return array
   */
  public function fetchAll(): array {
    $Statement = $this->prepare();
    $results = [];

    if ($this->Connector->isExecuted()) {
      $results = $Statement->fetchAll();

      if (!$results) {
        $results = [];
      }
    }

    return $results;
  }

  /**
   * Process PDO's fetch() method if Query Builder is set to COUNT. Return will be number of results.
   *
   * @return int
   */
  public function fetchCount(): int {
    $Statement = $this->prepare();
    $results = [];

    if ($this->Connector->isExecuted()) {
      $results = $Statement->fetch();
    }

    if (isset($results['count'])) {
      return $results['count'];
    }

    return 0;
  }

  /**
   * Runs PDOStatement's execute method.
   *
   * @param bool $returnId When set to true, return last added id.
   * @return bool|int
   */
  public function execute(bool $returnId = false) {
    $this->prepare();

    if ($returnId) {
      $result = $this->Connector->getConnection(true)->lastInsertId();
    } else {
      $result = $this->Connector->isExecuted();
    }

    $this->Connector->clean();

    return $result;
  }

  /**
   * Set bindings to builder.
   *
   * @param array $bindings Bindings to store.
   * @return $this
   */
  public function bind(array $bindings): Model {
    foreach ($bindings as $binding => $value) {
      $this->Builder->addBinding($binding, $value, true);
    }

    return $this;
  }

  /**
   * Prepare query and add bindings.
   *
   * @return PDOStatement
   */
  protected function prepare(): PDOStatement {
     $this->Connector
      ->prepareQuery($this->Builder->buildQuery()->getQuery())
      ->bind($this->Builder->getBindings());

     $this->Builder->clearBindings();

     return $this->Connector->execute();
  }

  /**
   * Check if table name is set. If not, generate name.
   */
  protected function verifyTableName() {
    if (!isset($this->tableName)) {
      $namespaces = explode('\\', get_class($this));
      $className = array_pop($namespaces);

      if (0 === substr_compare($className, 'Model', -5)) {
        $className = substr($className, 0, -5);
        $this->tableName = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $className));
      }
    }
  }

  /**
   * Creates correct Builder based on used engine.
   *
   * @return Model
   * @throws DbModelException
   */
  private function createBuilder(): Model {
    $Config = DatabaseConfig::getInstance();

    switch ($Config->engine) {
      case 'pgsql':
        $implementation = PgSqlBuilder::class;
        break;
      case 'mysql':
        $implementation = MySqlBuilder::class;
        break;
      default:
        throw new DbModelException(
          sprintf('Builder for engine "%s" does not exists.', $Config->engine),
          DbModelException::DF_ENGINE_BUILDER_DOES_NOT_EXISTS,
        );
    }

    try {
      $Builder = new BuilderFactory;
      $Builder->setImplementation($implementation);

      $this->Builder = $Builder->create();
    } catch (Exception $e) {
      ExceptionHandler::printOutput($e);
    }

    return $this;
  }

}
