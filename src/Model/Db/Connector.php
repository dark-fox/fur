<?php
namespace DarkFox\Fur\Model\Db;

use DarkFox\Fur\Core\ExceptionHandler;
use DarkFox\Fur\Model\Db\Exceptions\DbModelException;
use DarkFox\Fur\Tools\Singleton;
use Exception;
use PDO;
use PDOStatement;

class Connector extends Singleton
{
  private ?PDO $Connection = null;
  private ?PDOStatement $Statement = null;

  private bool $executed = false;

  /**
   * Connect to database using data stored in config file.
   *
   * @return Connector
   * @throws DbModelException
   */
  public function connect(): Connector {
    $Config = DatabaseConfig::getInstance();

    if ($Config->isEngineAvailable()) {
      try {
        $this->Connection = new PDO(
          sprintf('%s:host=%s;dbname=%s;port=%d', $Config->engine, $Config->host, $Config->name, $Config->port),
          $Config->user,
          $Config->password
        );
      } catch (Exception $e) {
        ExceptionHandler::printOutput($e);
      }
    } else {
      throw new DbModelException(
        sprintf('Engine "%s" is not supported.', $Config->engine),
        DbModelException::DF_ENGINE_NOT_SUPPORTED,
      );
    }

    return $this;
  }

  /**
   * Return PDO connection to database. When there is no active connection there is possibility to connect
   * automatically.
   *
   * @param bool $doConnect When set to true, force connection if there is none.
   * @return PDO
   */
  public function getConnection(bool $doConnect = true): ?PDO {
    if ($doConnect && is_null($this->Connection)) {
      try {
        $this->connect();
      } catch (Exception $e) {
        ExceptionHandler::printOutput($e);
      }
    }

    return $this->Connection;
  }

  /**
   * @param string $statement
   * @return Connector
   */
  public function prepareQuery(string $statement): Connector {
    $this->Statement = $this->Connection->prepare($statement);

    return $this;
  }

  /**
   * Executes given query.
   * @return PDOStatement|null
   */
  public function execute(): ?PDOStatement {
    $this->executed = $this->Statement->execute();
    return $this->Statement;
  }

  /**
   * Return value of executed property.
   *
   * @return bool
   */
  public function isExecuted(): bool {
    return $this->executed;
  }

  /**
   * Clean all temporary Connector variables.
   *
   * @return $this
   */
  public function clean(): Connector {
    $this->executed = false;
    return $this;
  }

  /**
   * Use PDO's bindValue() method on each values in query.
   *
   * @param array $bindings Values to bind.
   * @return Connector
   */
  public function bind(array $bindings): Connector {
    try {
       foreach ($bindings as $bind => $value) {
         $this->Statement->bindValue($bind, $value);
       }
    } catch (Exception $e) {
      ExceptionHandler::printOutput($e);
    }

    return $this;
  }

}
