<?php
namespace DarkFox\Fur\Model\Db;

use DarkFox\Fur\Config\Config;

class DatabaseConfig extends Config
{
  public string $engine;
  public string $host;
  public string $user;
  public string $password;
  public string $name;
  public int $port;

  private array $possibleEngines = [
    'pgsql',
    'mysql',
  ];

  /**
   * Boolean check if engine provided in setting file is correct.
   * @return bool
   */
  public function isEngineAvailable(): bool {
    return in_array($this->engine, $this->possibleEngines, true);
  }

}
