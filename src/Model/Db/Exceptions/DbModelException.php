<?php
namespace DarkFox\Fur\Model\Db\Exceptions;

use Exception;

class DbModelException extends Exception
{
  // Config
  public const DF_ENGINE_NOT_SUPPORTED = 100;
  public const DF_ENGINE_BUILDER_DOES_NOT_EXISTS = 101;

  // Connection
  public const DF_CONNECTION_IS_NULL = 200;

  // Builder
  public const DF_TABLE_NAME_IS_NOT_SET = 300;
  public const DF_UNSUPPORTED_JOIN_TYPE = 301;
  public const DF_UNSUPPORTED_QUERY_TYPE = 302;
  public const DF_NO_BUILDER_INSTANCE = 303;
  public const DF_ORDER_ARRAY_IS_EMPTY = 304;
  public const DF_WRONG_LIMIT = 305;
  public const DF_WRONG_OFFSET = 306;
  public const DF_NOTHING_TO_INSERT = 307;
  public const DF_NOTHING_TO_UPDATE = 308;
  public const DF_EMPTY_CUSTOM_QUERY = 309;

}
