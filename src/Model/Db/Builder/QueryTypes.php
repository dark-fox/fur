<?php
namespace DarkFox\Fur\Model\Db\Builder;

class QueryTypes
{
  public const QUERY_SELECT = 'select';
  public const QUERY_INSERT = 'insert';
  public const QUERY_UPSERT = 'upsert';
  public const QUERY_UPDATE = 'update';
  public const QUERY_DELETE = 'delete';
  public const QUERY_COUNT = 'count';
  public const QUERY_CUSTOM = 'custom';

  /**
   * Return an array filled with all const values.
   * @return array
   */
  public static function getTypes(): array {
    return [
      static::QUERY_SELECT,
      static::QUERY_INSERT,
      static::QUERY_UPSERT,
      static::QUERY_UPDATE,
      static::QUERY_DELETE,
      static::QUERY_COUNT,
      static::QUERY_CUSTOM,
    ];
  }

  /**
   * Check if given type is possible.
   *
   * @param string $type Type to check.
   * @return bool
   */
  public static function isTypeCorrect(string $type): bool {
    return in_array($type, static::getTypes(), true);
  }

}
