<?php
namespace DarkFox\Fur\Model\Db\Builder;

use DarkFox\Fur\Model\Db\Exceptions\DbModelException;

abstract class Builder implements IBuilder
{
  public const DEFAULT_LIMIT = 1;
  public const DEFAULT_OFFSET = 0;
  protected const TABLES_SCHEMA = 'public';

  protected array $bindings = [];
  protected string $join = '';
  protected string $limit = '';
  protected bool $limitlessResults = false;
  protected string $offset = '';
  protected string $order = '';
  protected string $group = '';
  protected string $tableName = '';
  protected string $where = '';
  protected string $query = '';
  protected string $queryType = '';

  protected array $supportedJoins = [
    'left' => 'LEFT JOIN',
    'right' => 'RIGHT JOIN',
    'inner' => 'INNER JOIN',
  ];

  /**
   * Set fields to initial values.
   *
   * @return $this
   */
  public function setValuesToDefault(): IBuilder {
    $this->limit = '';
    $this->join = '';
    $this->offset = '';
    $this->order = '';
    $this->group = '';
    $this->where = '';
    $this->limitlessResults = false;

    return $this;
  }

  /**
   * Clear bindings array.
   *
   * @return $this
   */
  public function clearBindings(): IBuilder {
    $this->bindings = [];
    return $this;
  }

  /**
   * Set name of current table.
   *
   * @param string $tableName Table name to set.
   * @return $this
   */
  public function setTableName(string $tableName): IBuilder {
    $this->tableName = $tableName;
    return $this;
  }

  /**
   * Set type of query.
   *
   * @param string $type Type of query, must exists in supported types array.
   * @return $this
   * @throws DbModelException
   */
  public function setQueryType(string $type): Builder {
    $this->verifyQueryType();
    $this->queryType = $type;

    return $this;
  }

  /**
   * Return type of current query.
   *
   * @return string
   */
  public function getQueryType(): string {
    return $this->queryType;
  }

  /**
   * Create correct syntax for JOIN statement.
   *
   * @param string $joinTable Name of the table to join.
   * @param string $joinField Field in joined table to bound with.
   * @param string $ownField  Name of field in current table to bound with.
   * @param string $type      Join type.
   * @return $this
   * @throws DbModelException
   */
  public function createJoin(string $joinTable, string $joinField, string $ownField, string $type = 'left'): Builder {
    if ('' === $this->tableName) {
      throw new DbModelException(
        'Table name must be set before join method is used.',
        DbModelException::DF_TABLE_NAME_IS_NOT_SET,
      );
    }

    if (array_key_exists($type, $this->supportedJoins)) {
      $joinType = $this->supportedJoins[$type];
    } else {
      throw new DbModelException(
        sprintf('Join with type %s is not supported.', $type),
        DbModelException::DF_UNSUPPORTED_JOIN_TYPE,
      );
    }

    $tableArray = explode('.', $joinTable);

    if (count($tableArray) === 1) {
      $joinTable = join('.', [
        static::TABLES_SCHEMA,
        $joinTable,
      ]);
    }

    $this->join = strip_tags(join(' ', [$joinType, $joinTable, 'ON', join('.', [$joinTable, $joinField]), '=', $ownField]));

    return $this;
  }

  /**
   * Create correct syntax for ORDER statement.
   *
   * @param array $fields
   * @return $this
   * @throws DbModelException
   */
  public function createOrder(array $fields): Builder {
    if (0 === count($fields)) {
      throw new DbModelException(
        'Fields property set in createOrder() method can\'t be empty array.',
        DbModelException::DF_ORDER_ARRAY_IS_EMPTY,
      );
    }

    $order = [];

    foreach ($fields as $field => $sort) {
      $order[] = sprintf('%s %s', $field, $sort);
    }

    $this->order = strip_tags(sprintf('ORDER BY %s', join(', ', $order)));

    return $this;
  }

  /**
   * Create correct syntax for ORDER statement.
   *
   * @param array $fields
   * @return $this
   * @throws DbModelException
   */
  public function createGroup(array $fields): Builder {
    if (0 === count($fields)) {
      throw new DbModelException(
        'Fields property set in createOrder() method can\'t be empty array.',
        DbModelException::DF_ORDER_ARRAY_IS_EMPTY,
      );
    }

    $group = [];

    foreach ($fields as $field => $sort) {
      $group[] = sprintf('%s %s', $field, $sort);
    }

    $this->group = strip_tags(sprintf('GROUP BY %s', join(', ', $group)));

    return $this;
  }

  /**
   * Create correct syntax for LIMIT statement.
   *
   * @param int $limit Results limit value.
   * @return $this
   * @throws DbModelException
   */
  public function createLimit(int $limit = self::DEFAULT_LIMIT): Builder {
    if (0 >= $limit) {
      throw new DbModelException(
        sprintf('Limit must be greater than 0, was: "%d".', $limit),
        DbModelException::DF_WRONG_LIMIT,
      );
    }

    $this->limit = sprintf('LIMIT %d', $limit);

    return $this;
  }

  /**
   * Create correct syntax for OFFSET statement.
   *
   * @param int $offset Value of offset.
   * @return $this
   * @throws DbModelException
   */
  public function createOffset(int $offset): Builder {
    if (0 > $offset) {
      throw new DbModelException(
        sprintf('Offset must be greater or equal than 0, was: "%d".', $offset),
        DbModelException::DF_WRONG_OFFSET,
      );
    }

    $this->offset = sprintf('OFFSET %d', $offset);

    return $this;
  }

  /**
   * Create correct syntax for SELECT statement.
   *
   * @param array $what Specify elements to select. When none will be set, all will fields will be returned.
   * @return $this
   */
  public function createSelect(array $what = []): Builder {
    if (0 === count($what)) {
      $selector = '*';
    } else {
      $selector = join(',', $what);
    }

    $this->queryType = QueryTypes::QUERY_SELECT;
    $this->query = sprintf('SELECT %s FROM %s', $selector, $this->tableName);

    return $this;
  }

  /**
   * Create correct syntax for INSERT statement.
   *
   * @param array $what Specify elements to insert to database.
   * @return Builder
   * @throws DbModelException
   */
  public function createInsert(array $what): Builder {
    if (0 === count($what)) {
      throw new DbModelException(
        'No value is specified to be added.',
        DbModelException::DF_NOTHING_TO_INSERT,
      );
    }

    $fields = join(',', array_keys($what));
    $binds = [];

    foreach ($what as $field => $value) {
      $binding = $this->convertToBinding($field);
      $this->addBinding($binding, $value);
      $binds[] = $binding;
    }

    $this->queryType = QueryTypes::QUERY_INSERT;
    $this->query = sprintf('INSERT INTO %s (%s) VALUES (%s)', $this->tableName, $fields, join(',', $binds));

    return $this;
  }

  /**
   * Create correct syntax for UPDATE statement.
   *
   * @param array $what Specify elements to insert to database.
   * @return Builder
   * @throws DbModelException
   */
  public function createUpdate(array $what): Builder {
    if (0 === count($what)) {
      throw new DbModelException(
        'No value is specified to be updated.',
        DbModelException::DF_NOTHING_TO_UPDATE,
      );
    }

    $binds = [];

    foreach ($what as $field => $value) {
      $binding = $this->convertToBinding($field);
      $this->addBinding($binding, $value);
      $binds[] = sprintf('%s = %s', $field, $binding);
    }

    $this->queryType = QueryTypes::QUERY_UPDATE;
    $this->query = sprintf('UPDATE %s SET %s', $this->tableName, join(',', $binds));

    return $this;
  }

  /**
   * Create correct syntax for DELETE statement.
   *
   * @return Builder
   */
  public function createDelete(): Builder {
    $this->queryType = QueryTypes::QUERY_DELETE;
    $this->query = sprintf('DELETE FROM %s', $this->tableName);

    return $this;
  }

  /**
   * Create correct syntax for SELECT count(*) statement.
   *
   * @return Builder
   */
  public function createCount(): Builder {
    $this->createSelect(['count(*)']);
    $this->queryType = QueryTypes::QUERY_COUNT;

    return $this;
  }

  /**
   * Add possibility to process custom query.
   *
   * @param string $query Query to process
   * @return Builder
   * @throws DbModelException
   */
  public function createCustom(string $query): Builder {
    if (0 === strlen($query)) {
      throw new DbModelException(
        'No query is provided to execute.',
        DbModelException::DF_EMPTY_CUSTOM_QUERY,
      );
    }

    $this->queryType = QueryTypes::QUERY_CUSTOM;
    $this->query = $query;

    return $this;
  }

  /**
   * Set WHERE part of the query.
   *
   * @param string $where All WHERE arguments.
   * @return Builder
   */
  public function createWhere(string $where): Builder {
    $this->where = sprintf('WHERE %s', $where);
    return $this;
  }

  /**
   * Add new position to bindings array.
   *
   * @param string $binding Name of the binding.
   * @param mixed  $value   Its value.
   * @param bool   $replace When set to true, replace value if doesn't exists.
   * @return Builder
   */
  public function addBinding(string $binding, $value, $replace = false): Builder {
    if (!$replace && !isset($this->bindings[$binding]) || $replace) {
      $this->bindings[$binding] = $value;
    }

    return $this;
  }

  /**
   * Return array with bindings.
   *
   * @return array
   */
  public function getBindings(): array {
    return $this->bindings;
  }

  /**
   * Set new value for whole query.
   *
   * @param string $query New query value.
   * @return $this
   */
  public function setQuery(string $query): Builder {
    $this->query = $query;
    return $this;
  }

  /**
   * Return current value of query.
   *
   * @return string
   */
  public function getQuery(): string {
    return $this->query;
  }

  /**
   * Set query property value to default, empty state.
   *
   * @return $this
   */
  public function cleanQuery(): Builder {
    $this->query = '';

    return $this;
  }

  /**
   * Main function that build query to execute.
   *
   * @return Builder
   * @throws DbModelException
   */
  public function buildQuery(): Builder {
    $this->verifyQueryType()->verifyLimit();

    switch ($this->queryType) {
      case QueryTypes::QUERY_SELECT:
      case QueryTypes::QUERY_COUNT:
        $this->extendQuery($this->join)->extendQuery($this->where)->extendQuery($this->group)->extendQuery($this->order)->extendQuery($this->limit)->extendQuery($this->offset);
        break;
      case QueryTypes::QUERY_UPDATE:
      case QueryTypes::QUERY_DELETE:
        $this->extendQuery($this->where);
        break;
    }

    $this->setValuesToDefault();

    return $this;
  }

  /**
   * Enable LIMIT statement for current query.
   *
   * @return $this
   */
  public function enableLimit(): Builder {
    $this->limitlessResults = false;
    return $this;
  }

  /**
   * Disable LIMIT statement for current query.
   *
   * @return $this
   */
  public function disableLimit(): Builder {
    $this->limitlessResults = true;
    return $this;
  }

  /**
   * Adds content to query if it's not empty.
   * @param string $content Content to add.
   * @return Builder
   */
  protected function extendQuery(string $content): Builder {
    if ('' !== $content) {
      $this->query .= ' '.$content;
    }

    return $this;
  }

  /**
   * Convert given name to correct binding notation.
   *
   * @param string $name Value to convert.
   * @return string
   */
  protected function convertToBinding(string $name): string {
    return vsprintf(':%s', [
      mb_strtolower($name),
    ]);
  }

  /**
   * Throw an DbModelException if query has wrong type.
   *
   * @return $this
   * @throws DbModelException
   */
  protected function verifyQueryType(): Builder {
    if (!QueryTypes::isTypeCorrect($this->queryType)) {
      throw new DbModelException(
        sprintf('Type "%s" is not supported.', $this->queryType),
        DbModelException::DF_UNSUPPORTED_QUERY_TYPE,
      );
    }

    return $this;
  }

  /**
   * Check if we're able to add LIMIT to the query.
   *
   * @return $this
   * @throws DbModelException
   */
  protected function verifyLimit(): Builder {
    if (!$this->limitlessResults && '' === $this->limit) {
      $this->createLimit();
    }

    return $this;
  }

}
