<?php
namespace DarkFox\Fur\Model\Db\Builder;

interface IBuilder
{
  public function setValuesToDefault(): IBuilder;
  public function clearBindings(): IBuilder;
  public function setTableName(string $tableName): IBuilder;

  public function setQueryType(string $type): IBuilder;
  public function getQueryType(): string;

  public function createJoin(string $joinTable, string $joinField, string $ownField, string $type = 'left'): IBuilder;
  public function createOrder(array $fields): IBuilder;
  public function createGroup(array $fields): IBuilder;
  public function createLimit(int $limit): IBuilder;
  public function createOffset(int $offset): IBuilder;
  public function createSelect(array $what = []): IBuilder;
  public function createInsert(array $what): IBuilder;
  public function createUpdate(array $what): IBuilder;
  public function createDelete(): IBuilder;
  public function createCount(): IBuilder;
  public function createWhere(string $where): IBuilder;

  public function addBinding(string $binding, $value, $replace = false): IBuilder;
  public function getBindings(): array;

  public function setQuery(string $query): IBuilder;
  public function getQuery(): string ;
  public function cleanQuery(): IBuilder;
  public function buildQuery(): IBuilder;

  public function enableLimit(): IBuilder;
  public function disableLimit(): IBuilder;

}
