<?php
namespace DarkFox\Fur\View;

use DarkFox\Fur\Core\ExceptionHandler;
use DarkFox\Fur\Tools\Dir;
use Exception;
use ReflectionClass;
use ReflectionProperty;

abstract class View
{
  protected const TEMPLATE_EXTENSION = '.tpl.php';

  public array $js = [];
  public array $css = [];

  private ?ReflectionClass $Reflection = null;
  private string $templateFilePath;
  private array $views = [];
  private array $viewsPaths = [];

  public function __construct() {
    $this
      ->createReflection()
      ->getTemplateFilePath()
      ->storeViews($this);
  }

  public function run(): void {}

  public function getHtml(): string {
    $this->run();
    $this->loadResources();

    ob_start();

    if (file_exists($this->templateFilePath)) {
      extract($this->getVariables());

      include $this->templateFilePath;
    }

    return ob_get_clean();
  }

  public function render(): void {
    echo $this->getHtml();
  }

  public function set(array $values = null): View {
    foreach ($values as $variable => $value) {
      if ($this->Reflection->hasProperty($variable)) {
        $isPropertyProtected = false;

        $property = $this->Reflection->getProperty($variable);

        if ($property->isProtected()) {
          $property->setAccessible(true);
          $isPropertyProtected = true;
        }

        $property->setValue($this, $value);

        if ($isPropertyProtected) {
          $property->setAccessible(false);
        }
      }
    }

    return $this;
  }

  public function addView(View $View, string $variable): View {
    $this->storeViews($View);
    $content = $View->getHtml();

    $this->applyResources($View);
    $this->set([$variable => $content]);

    return $this;
  }

  protected function getViews(): array{
    return $this->views;
  }

  protected function addCss(string $path) {
    if (file_exists($path)) {
      $this->css[$path] = sprintf('<link href="/%s" rel="stylesheet">', $path);
    }
  }

  protected function addJs(string $path) {
    if (file_exists($path)) {
      $this->js[$path] = sprintf('<script src="/%s"></script>', $path);
    }
  }

  protected function applyResources(View $View): View {
    $this->js = array_merge($this->js, $View->js);
    $this->css = array_merge($this->css, $View->css);

    return $this;
  }

  private function createReflection(): View
  {
    if (is_null($this->Reflection)) {
      try {
        $this->Reflection = new ReflectionClass($this);
      } catch (Exception $e) {
        ExceptionHandler::printOutput($e);
      }
    }

    return $this;
  }

  private function getTemplateFilePath(): View {
    $classFileName = $this->Reflection->getFileName();
    $this->templateFilePath = str_replace('.php', static::TEMPLATE_EXTENSION, $classFileName);

    return $this;
  }

  private function getVariables(): array {
    $properties = $this->Reflection->getProperties(ReflectionProperty::IS_PUBLIC);
    $variables = [];

    if (is_array($properties)) {
      foreach ($properties as $property) {
        $variables[$property->getName()] = $property->getValue($this);
      }
    }

    return $variables;
  }

  private function storeViews(View $View) {
    try {
      $reflection = new ReflectionClass($View);
      $viewPath = $reflection->getFileName();
      $viewNamespace = $reflection->getName();

      if (!in_array($viewNamespace, $this->views)) {
        $this->views[] = $viewNamespace;
      }

      if (!in_array($viewPath, $this->viewsPaths)) {
        $this->viewsPaths[] = $viewPath;
      }
    } catch (Exception $e) {
      ExceptionHandler::printOutput($e);
    }
  }

  private function loadResources() {
    foreach ($this->viewsPaths as $view) {
      $this->tryLoad($view, 'css');
      $this->tryLoad($view, 'js');
    }
  }

  private function tryLoad(string $path, string $extension) {
    try {
      $fullPath = join('.', [
        str_replace('.php', '', $path),
        $extension,
      ]);
      $basePath = Dir::getRealPath($this, DIRECTORY_SEPARATOR);

      if (file_exists($fullPath)) {
        $basePath = join('.', [
          $basePath,
          $extension,
        ]);

        if ($extension == 'css') {
          $this->addCss($basePath);
        } elseif ($extension == 'js') {
          $this->addJs($basePath);
        }
      }
    } catch (Exception $e) {
      ExceptionHandler::printOutput($e);
    }
  }

}

