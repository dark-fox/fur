<?php
namespace DarkFox\Fur\View\Exceptions;

use Exception;

class ViewException extends Exception
{
  public const DF_VALUES_EXCEPTION_NULL_AS_STRING = 1;
  public const DF_VALUES_JSON_ERROR = 2;

}
