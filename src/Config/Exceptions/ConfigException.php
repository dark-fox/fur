<?php
namespace DarkFox\Fur\Config\Exceptions;

use Exception;

class ConfigException extends Exception
{
  public const DF_FILE_NOT_EXISTS = 1;
  public const DF_FILE_IS_NOT_READABLE = 2;
  public const DF_FILE_IS_NOT_VALID_JSON = 3;
  public const DF_CONFIG_IS_NOT_ITERABLE = 4;

}
