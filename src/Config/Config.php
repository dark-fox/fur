<?php
namespace DarkFox\Fur\Config;

use DarkFox\Fur\Config\Exceptions\ConfigException;
use DarkFox\Fur\Core\ExceptionHandler;
use DarkFox\Fur\Tools\Dir;
use DarkFox\Fur\Tools\Singleton;
use Exception;
use ReflectionClass;
use ReflectionException;

abstract class Config extends Singleton
{
  private const EXTENSION = '.json';
  private const CONFIG_DIR = 'config';
  private const CONFIG = 'Config';

  private string $className;
  private string $pathAbsolute;
  private string $configFile;
  private string $configPath;
  private array $config;

  /**
   * Config constructor.
   * @throws Exception
   */
  public function __construct() {
    $this
      ->setClass()
      ->setPathAbsolute()
      ->setConfig()
      ->loadConfigFile()
      ->assign();
  }

  /**
   * @return Config
   * @throws ReflectionException
   */
  private function setClass(): Config {
    $classReflection = new ReflectionClass($this);
    $this->className = $classReflection->getShortName();

    return $this;
  }

  /**
   * @return Config
   */
  private function setPathAbsolute(): Config {
    try {
      $this->pathAbsolute = join('', [
        Dir::getAbsolute($this),
        self::CONFIG_DIR,
      ]);
    } catch (Exception $e) {
      ExceptionHandler::printOutput($e);
    }

    return $this;
  }

  private function setConfig(): Config {
    $this->configFile = join('', [
      mb_strtolower(str_replace(self::CONFIG, '', $this->className)),
      self::EXTENSION,
    ]);
    $this->configPath = join(DIRECTORY_SEPARATOR, [
      $this->pathAbsolute,
      $this->configFile,
    ]);

    return $this;
  }

  /**
   * @return Config
   * @throws ConfigException
   */
  private function loadConfigFile(): Config {
    if (!file_exists($this->configPath)) {
      throw new ConfigException(sprintf('Config file "%s" not exists in path %s.', $this->configFile, $this->pathAbsolute), ConfigException::DF_FILE_NOT_EXISTS);
    }

    if (!is_readable($this->configPath)) {
      throw new ConfigException(sprintf('Config file "%s" is not readable.', $this->configFile), ConfigException::DF_FILE_IS_NOT_READABLE);
    }

    $content = file_get_contents($this->configPath);
    $this->config = json_decode($content, true);

    if (is_null($this->config)) {
      throw new ConfigException(sprintf('Config file "%s" is not valid JSON. Error: %s', $this->configFile, json_last_error_msg()), ConfigException::DF_FILE_IS_NOT_VALID_JSON);
    }

    return $this;
  }

  /**
   * @return Config
   * @throws ConfigException
   */
  private function assign(): Config {
    $fields = get_class_vars(get_class($this));

    if (is_iterable($this->config)) {
      foreach ($this->config as $field => $value) {
        if (array_key_exists($field, $fields)) {
          $this->{$field} = $value;
        }
      }
    } else {
      throw new ConfigException(sprintf('Config file "%s" should be iterable, not %s.', $this->configFile, gettype($this->config)), ConfigException::DF_CONFIG_IS_NOT_ITERABLE);
    }

    return $this;
  }

}
