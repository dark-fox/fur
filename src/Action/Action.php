<?php
namespace DarkFox\Fur\Action;

use DarkFox\Fur\Data\Request;

abstract class Action
{
  protected Request $Request;

  public function __construct() {
    $this->Request = new Request;
  }

  abstract public function run(): void;

}
