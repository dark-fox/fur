<?php
namespace DarkFox\Fur\Action;

abstract class AsynchronousAction extends Action
{
  protected const HEADER_JSON = 'Content-Type: application/json';

  protected bool $die = true;
  protected bool $checkRequest = true;

  private string $response = '{}';

  public function __construct()
  {
    $this->checkRequest();
    $this->setHeader();
    parent::__construct();
  }

  public function __destruct()
  {
    if ($this->die === true) {
      die($this->response);
    }

    echo $this->response;
  }

  protected function setResponse(string $response): void
  {
    $this->response = $response;
  }

  protected function checkRequest(): void
  {
    if ($this->checkRequest && (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest')) {
      $this->setResponse('not correct request');
      die;
    }
  }

  protected function setHeader(): void
  {
    header(static::HEADER_JSON);
  }

}
