<?php
namespace DarkFox\Fur\Action\Exceptions;

use Exception;

class ViewActionException extends Exception
{
  public const DF_VIEW_ACTION_DATA_KEY_DOES_NOT_EXISTS = 1;
  public const DF_VIEW_ACTION_DATA_KEY_ID_DOES_NOT_EXISTS = 2;
  public const DF_VIEW_ACTION_ID_DOES_NOT_EXISTS_IN_QUERY = 3;

}
