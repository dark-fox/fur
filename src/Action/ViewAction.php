<?php
namespace DarkFox\Fur\Action;

use DarkFox\Fur\Action\Exceptions\ViewActionException;
use DarkFox\Fur\Core\ExceptionHandler;
use DarkFox\Fur\Data\Values;
use DarkFox\Fur\View\View;
use Exception;
use ReflectionClass;

abstract class ViewAction extends Action
{
  protected array $data = [];
  protected string $layout;

  private ?View $Layout = null;
  private Values $Values;
  private array $query = [];

  public function __construct() {
    parent::__construct();

    $this->setQuery();

    $this->Values = new Values;

    if (is_string($this->layout) && class_exists($this->layout)) {
      try {
        $Reflection = new ReflectionClass($this->layout);
        $View = $Reflection->newInstance();

        if ($View instanceof View) {
          $this->setLayout($View);
        }
      } catch (Exception $e) {
        ExceptionHandler::printOutput($e);
      }
    }
  }

  /**
   * Display final HTML.
   */
  public function __destruct() {
    if ($this->Layout instanceof View) {
      $this->Layout->render();
    }
  }

  /**
   * Allows to overwrite data array.
   * @param array $data New data to set.
   * @return ViewAction
   */
  public function setData(array $data): ViewAction {
    $this->data = $data;
    return $this;
  }

  /**
   * Allows to overwrite query parameters set to the contoller.
   * @param array $query New additional query data to set to the Controller.
   * @return ViewAction
   */
  public function overwriteQuery(array $query): ViewAction {
    $this->query = $query;
    return $this;
  }

  /**
   * Assign View to layout.
   *
   * @param View $View
   */
  protected function setLayout(View $View): void {
    $this->Layout = $View;
  }

  /**
   * Add page to layout.
   *
   * @param View   $View
   * @param string $variableName
   * @return ViewAction
   */
  protected function addPage(View $View, string $variableName): ViewAction {
    $this->Layout->addView($View, $variableName);
    return $this;
  }

  /**
   * Get query data.
   *
   * @param string $key Key to get from data.
   * @return Values
   * @throws ViewActionException
   */
  protected function getData(string $key): Values {
    if (isset($this->data[$key])) {
      if (isset($this->data[$key]['id'])) {
        $id = $this->data[$key]['id'];

        if (isset($this->query[$id]) || (!isset($this->query[$id]) && isset($this->data[$key]['default']))) {
          $value = null;

          if (isset($this->query[$id]) && $this->query[$id]) {
            $value = $this->query[$id];
          } elseif (isset($this->data[$key]['default'])) {
            $value = $this->data[$key]['default'];
          }

          return $this->Values->setValue($value);
        } else {
          throw new ViewActionException(
            sprintf('Id "%s" does not exits in query and default value is not set.', $id),
            ViewActionException::DF_VIEW_ACTION_ID_DOES_NOT_EXISTS_IN_QUERY,
          );
        }
      } else {
        throw new ViewActionException(
          sprintf('Id for key "%s" is not set in data array.', $key),
          ViewActionException::DF_VIEW_ACTION_DATA_KEY_ID_DOES_NOT_EXISTS,
        );
      }
    } else {
      throw new ViewActionException(
        sprintf('Key "%s" is not set in data array.', $key),
        ViewActionException::DF_VIEW_ACTION_DATA_KEY_DOES_NOT_EXISTS,
      );
    }
  }

  /**
   * Return query array.
   *
   * @return array
   */
  protected function getQuery(): array {
    return $this->query;
  }

  /**
   * Assign query value to class property.
   *
   * @return ViewAction
   */
  private function setQuery(): ViewAction {
    try {
      $this->query = array_slice($this->Request->query(), 2);
    } catch (Exception $exception) {
      ExceptionHandler::printOutput($exception);
    }

    return $this;
  }

}
