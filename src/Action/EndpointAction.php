<?php
namespace DarkFox\Fur\Action;

abstract class EndpointAction extends Action
{
  protected bool $die = true;

  private string $response = '{}';

  public function __destruct() {
    if ($this->die === true) {
      die($this->response);
    }

    echo $this->response;
  }

  protected function setResponse(string $response): void {
    $this->response = $response;
  }

}
