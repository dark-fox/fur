<?php
namespace DarkFox\Fur\Random;

use DarkFox\Fur\Tools\Singleton;

class Random extends Singleton
{
  /**
   * Generate random string.
   *
   * @param int  $length     Determines how long should be generated string.
   * @param bool $useDigits  Should string contains digits?
   * @param bool $upperCase  Should string contains both lower and uppercase characters?
   * @param bool $useSpecial Should string contains special characters?
   * @return string
   */
  public function string(int $length = 9, bool $useDigits = true, bool $upperCase = true, bool $useSpecial = true): string {
    $chars = [
      'digits' => '0123456789',
      'characters' => 'abcdefghijklmnopqrstuvwxyz',
      'special' => '!@#$%^&*(){}[];:<>,./?=+-_',
    ];
    $set = $chars['characters'];

    if ($useDigits) {
      $set = $chars['digits'].$chars['characters'];
    }

    if ($upperCase) {
      $set .= mb_strtoupper($chars['characters']);
    }

    if ($useSpecial) {
      $set .= $chars['special'];
    }

    $result = '';
    $setLength = strlen($set);

    for ($i = 0; $i < $length; $i++) {
      $result .= $set[mt_rand(0, $setLength - 1)];
    }

    return $result;
  }

}
