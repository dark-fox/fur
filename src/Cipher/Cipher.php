<?php
namespace DarkFox\Fur\Cipher;

use DarkFox\Fur\Tools\Singleton;

class Cipher extends Singleton
{
  /**
   * Encrypts given content.
   *
   * @param string $content Content to encrypt.
   * @return string
   */
  public function encrypt(string $content): string {
    $Config = CipherConfig::getInstance();

    $encryptedContent = openssl_encrypt(
      $content,
      $Config->cipher,
      $Config->key,
      0,
      $Config->iv
    );

    if (false === $encryptedContent) {
      $encryptedContent = '';
    }

    return $encryptedContent;
  }

  /**
   * Decrypts given content.
   *
   * @param string $content Content to decrypt.
   * @return string
   */
  public function decrypt(string $content): string {
    $Config = CipherConfig::getInstance();

    $decryptedContent = openssl_decrypt(
      $content,
      $Config->cipher,
      $Config->key,
      0,
      $Config->iv
    );

    if (false === $decryptedContent) {
      $decryptedContent = '';
    }

    return $decryptedContent;
  }
}
