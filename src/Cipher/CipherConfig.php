<?php
namespace DarkFox\Fur\Cipher;

use DarkFox\Fur\Config\Config;

class CipherConfig extends Config
{
  public string $cipher;
  public string $key;
  public string $iv;

}
