<?php
namespace DarkFox\Fur\Lang\Exceptions;

use Exception;

class LangException extends Exception
{
  const DF_LANG_NO_FILE = 100;
  const DF_LANG_JSON_ERROR = 101;

}
