<?php
namespace DarkFox\Fur\Lang;

use DarkFox\Fur\Core\ExceptionHandler;
use DarkFox\Fur\Data\Exceptions\ValuesException;
use DarkFox\Fur\Data\Request;
use DarkFox\Fur\Lang\Exceptions\LangException;
use DarkFox\Fur\Tools\Dir;
use DarkFox\Fur\Tools\Singleton;
use Exception;

class Lang extends Singleton
{
  protected const LANGUAGES_DIR = 'languages';
  protected const COOKIE_NAME = 'dff_lang';

  private ?array $translations = [];

  /**
   * Translate given phrase.
   *
   * @param string      $namespace Translation namespace.
   * @param string      $path      Exact phrase that will be translated.
   * @param string|null $default   Optional default output if translation will not be found.
   * @return string
   */
  public function translate(string $namespace, string $path, string $default = null): string {
    $this->checkTranslations();

    $translation = join('.', [$namespace, $path]);

    if (isset($this->translations[$namespace][$path])) {
      $translation = $this->translations[$namespace][$path];
    } elseif (!is_null($default)) {
      $translation = $default;
    }

    return  $translation;
  }

  /**
   * Check if translations are downloaded.
   *
   * @return $this
   */
  protected function checkTranslations(): Lang {
    if (0 === count($this->translations)) {
      $this->getTranslations();
    }

    return $this;
  }

  /**
   * Assign translations from file to variable.
   *
   * @return $this
   */
  protected function getTranslations(): Lang {
    try {
      $file = join('/', [
        Dir::getAbsolute($this),
        static::LANGUAGES_DIR,
        join('.', [$this->getLanguage(), 'json']),
      ]);

      if (file_exists($file)) {
        $translations = file_get_contents($file);
        $this->translations = json_decode($translations, true);

        if (!is_array($this->translations)) {
          throw new LangException(
            sprintf('File "%s" has json error: %s', $file, json_last_error_msg()),
            LangException::DF_LANG_JSON_ERROR,
          );
        }
      } else {
        throw new LangException(
          sprintf('File "%s" does not exits.', $file),
          LangException::DF_LANG_NO_FILE,
        );
      }
    } catch (Exception $exception) {
      ExceptionHandler::printOutput($exception);
    }

    return $this;
  }

  /**
   * Check cookie for a language name. If cookie doesn't exits, get data from config.
   *
   * @return string
   */
  protected function getLanguage(): string {
    try {
      $Request = new Request;
      $language = $Request->cookie(static::COOKIE_NAME)->string();
    } catch (ValuesException $exception) {
      $language = null;
    }

    if (is_null($language)) {
      $language = LangConfig::getInstance()->defaultLanguage;
    }

    return $language;
  }

}
