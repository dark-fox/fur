<?php
namespace DarkFox\Fur\Validation\Validators;

use DarkFox\Fur\Validation\ErrorCodes;
use DarkFox\Fur\Validation\Errors;

abstract class BaseValidator
{
  protected $value;
  protected Errors $Errors;

  /**
   * BaseValidator constructor.
   *
   * @param mixed  $value  Value to test.
   * @param Errors $Errors Validation errors handler instance.
   */
  public function __construct($value, Errors $Errors) {
    $this->value = $value;
    $this->Errors = $Errors;
  }

  /**
   * Check given value with expected one.
   *
   * @param mixed $expected Expected value to check.
   * @return $this
   */
  public function hasValue($expected): self {
    if ($this->value !== $expected) {
      $this->Errors->addError(
        'VALUES_ARE_NOT_EQUAL',
        ErrorCodes::VALUES_ARE_NOT_EQUAL,
      );
    }

    return $this;
  }

  /**
   * Check given value is different than expected one.
   *
   * @param mixed $expected Expected value to check.
   * @return $this
   */
  public function isDifferentThan($expected): self {
    if ($this->value === $expected) {
      $this->Errors->addError(
        'VALUES_ARE_EQUAL',
        ErrorCodes::VALUES_ARE_EQUAL,
      );
    }

    return $this;
  }

}
