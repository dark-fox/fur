<?php
namespace DarkFox\Fur\Validation\Validators;

use DarkFox\Fur\Validation\ErrorCodes;

class BooleanValidator extends BaseValidator
{
  /**
   * Check if tested value is true.
   *
   * @return $this
   */
  public function isTrue(): BooleanValidator {
    if (false === $this->value) {
      $this->Errors->addError(
        'BOOLEAN_IS_NOT_TRUE',
        ErrorCodes::BOOLEAN_IS_NOT_TRUE,
      );
    }

    return $this;
  }

  /**
   * Check if tested value is false.
   *
   * @return $this
   */
  public function isFalse(): BooleanValidator {
    if (true === $this->value) {
      $this->Errors->addError(
        'BOOLEAN_IS_NOT_FALSE',
        ErrorCodes::BOOLEAN_IS_NOT_FALSE,
      );
    }

    return $this;
  }

}
