<?php
namespace DarkFox\Fur\Validation\Validators;

use DarkFox\Fur\Validation\ErrorCodes;

class IntegerValidator extends BaseValidator
{
  /**
   * Check if tested integer is greater than expected.
   *
   * @param int $expected Integer to check.
   * @return $this
   */
  public function isGreater(int $expected): IntegerValidator {
    if ($expected >= $this->value) {
      $this->Errors->addError(
        'INTEGER_SHOULD_BE_GREATER',
        ErrorCodes::INTEGER_SHOULD_BE_GREATER,
      );
    }

    return $this;
  }

  /**
   * Check if tested integer is greater or equal than expected.
   *
   * @param int $expected Integer to check.
   * @return $this
   */
  public function isGreaterOrEqual(int $expected): IntegerValidator {
    if ($expected > $this->value) {
      $this->Errors->addError(
        'INTEGER_SHOULD_BE_GREATER_OR_EQUAL',
        ErrorCodes::INTEGER_SHOULD_BE_GREATER_OR_EQUAL,
      );
    }

    return $this;
  }

  /**
   * Check if tested integer is lower than expected.
   *
   * @param int $expected Integer to check.
   * @return $this
   */
  public function isLower(int $expected): IntegerValidator {
    if ($expected <= $this->value) {
      $this->Errors->addError(
        'INTEGER_SHOULD_BE_LOWER',
        ErrorCodes::INTEGER_SHOULD_BE_LOWER,
      );
    }

    return $this;
  }

  /**
   * Check if tested integer is lower or equal than expected.
   *
   * @param int $expected Integer to check.
   * @return $this
   */
  public function isLowerOrEqual(int $expected): IntegerValidator {
    if ($expected < $this->value) {
      $this->Errors->addError(
        'INTEGER_SHOULD_BE_LOWER_OR_EQUAL',
        ErrorCodes::INTEGER_SHOULD_BE_LOWER_OR_EQUAL,
      );
    }

    return $this;
  }

  /**
   * Check if tested integer is divided by given value.
   *
   * @param int|float $divider Divider to check.
   * @return $this
   */
  public function isDividedBy($divider): IntegerValidator {
    if (!is_int($divider) && !is_float($divider)) {
      $this->Errors->addError(
        'DIVIDER_SHOULD_BE_INTEGER_OR_FLOAT',
        ErrorCodes::DIVIDER_SHOULD_BE_INTEGER_OR_FLOAT,
      );
    }

    if (0 !== $this->value % $divider) {
      $this->Errors->addError(
        'INTEGER_IS_NOT_DIVIDED',
        ErrorCodes::INTEGER_IS_NOT_DIVIDED,
      );
    }

    return $this;
  }

  /**
   * Check if tested integer is not divided by given value.
   *
   * @param int|float $divider Divider to check.
   * @return $this
   */
  public function isNotDividedBy($divider): IntegerValidator {
    if (!is_int($divider) && !is_float($divider)) {
      $this->Errors->addError(
        'DIVIDER_SHOULD_BE_INTEGER_OR_FLOAT',
        ErrorCodes::DIVIDER_SHOULD_BE_INTEGER_OR_FLOAT,
      );
    }

    if (0 === $this->value % $divider) {
      $this->Errors->addError(
        'INTEGER_IS_DIVIDED',
        ErrorCodes::INTEGER_IS_DIVIDED,
      );
    }

    return $this;
  }

  /**
   * Check if tested integer is even.
   *
   * @return $this
   */
  public function isEven(): IntegerValidator {
    if (0 !== $this->value % 2) {
      $this->Errors->addError(
        'INTEGER_IS_NOT_EVEN',
        ErrorCodes::INTEGER_IS_NOT_EVEN,
      );
    }

    return $this;
  }

  /**
   * Check if tested integer is odd.
   *
   * @return $this
   */
  public function isOdd(): IntegerValidator {
    if (0 === $this->value % 2) {
      $this->Errors->addError(
        'INTEGER_IS_NOT_ODD',
        ErrorCodes::INTEGER_IS_NOT_ODD,
      );
    }

    return $this;
  }

}
