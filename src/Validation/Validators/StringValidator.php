<?php
namespace DarkFox\Fur\Validation\Validators;

use DarkFox\Fur\Validation\ErrorCodes;

class StringValidator extends BaseValidator
{
  /**
   * Check if tested string has length as expected.
   *
   * @param int $expected String length to check.
   * @return $this
   */
  public function hasLength(int $expected): StringValidator {
    if ($expected !== mb_strlen($this->value)) {
      $this->Errors->addError(
        'WRONG_LENGTH',
        ErrorCodes::WRONG_LENGTH,
      );
    }

    return $this;
  }

  /**
   * Check if tested string is longer than expected.
   *
   * @param int $expected String length to check.
   * @return $this
   */
  public function isLongerThan(int $expected): StringValidator {
    if ($expected >= mb_strlen($this->value)) {
      $this->Errors->addError(
        'STRING_SHOULD_BE_LONGER',
        ErrorCodes::STRING_SHOULD_BE_LONGER,
      );
    }

    return $this;
  }

  /**
   * Check if tested string is longer than expected.
   *
   * @param int $expected String length to check.
   * @return $this
   */
  public function isLongerThanOrEqual(int $expected): StringValidator {
    if ($expected > mb_strlen($this->value)) {
      $this->Errors->addError(
        'STRING_SHOULD_BE_LONGER_OR_EQUAL',
        ErrorCodes::STRING_SHOULD_BE_LONGER_OR_EQUAL,
      );
    }

    return $this;
  }

  /**
   * Check if tested string is shorter than expected.
   *
   * @param int $expected String length to check.
   * @return $this
   */
  public function isShorterThan(int $expected): StringValidator {
    if ($expected <= mb_strlen($this->value)) {
      $this->Errors->addError(
        'STRING_SHOULD_BE_SHORTER',
        ErrorCodes::STRING_SHOULD_BE_SHORTER,
      );
    }

    return $this;
  }

  /**
   * Check if tested string is shorter or equal than expected.
   *
   * @param int $expected String length to check.
   * @return $this
   */
  public function isShorterThanOrEqual(int $expected): StringValidator {
    if ($expected < mb_strlen($this->value)) {
      $this->Errors->addError(
        'STRING_SHOULD_BE_SHORTER_OR_EQUAL',
        ErrorCodes::STRING_SHOULD_BE_SHORTER_OR_EQUAL,
      );
    }

    return $this;
  }

  /**
   * Check if expected string is in tested value.
   *
   * @param string $expected String to check.
   * @return $this
   */
  public function contains(string $expected): StringValidator {
    if (false === mb_strpos($this->value, $expected)) {
      $this->Errors->addError(
        'STRING_DOES_NOT_CONTAIN',
        ErrorCodes::STRING_DOES_NOT_CONTAIN,
      );
    }

    return $this;
  }

  /**
   * Check if expected string isn't in tested value.
   *
   * @param string $expected String to check.
   * @return $this
   */
  public function doesNotContain(string $expected): StringValidator {
    if (false !== mb_strpos($this->value, $expected)) {
      $this->Errors->addError(
        'STRING_DOES_CONTAIN',
        ErrorCodes::STRING_DOES_CONTAIN,
      );
    }

    return $this;
  }

  /**
   * Check if tested value is correct E-Mail address.
   *
   * @return $this
   */
  public function isEmail(): StringValidator {
    if (!filter_var($this->value, FILTER_VALIDATE_EMAIL)) {
      $this->Errors->addError(
        'STRING_IS_NOT_AN_EMAIL',
        ErrorCodes::STRING_IS_NOT_AN_EMAIL,
      );
    }

    return $this;
  }

  /**
   * Check if tested value is correct URL.
   *
   * @return $this
   */
  public function isURL(): StringValidator {
    if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $this->value)) {
      $this->Errors->addError(
        'STRING_IS_NOT_A_URL',
        ErrorCodes::STRING_IS_NOT_A_URL,
      );
    }

    return $this;
  }

  /**
   * Check if tested value is in given array.
   *
   * @param array $expected Array to check.
   * @return $this
   */
  public function isInArray(array $expected): StringValidator {
    if (!in_array($this->value, $expected, true)) {
      $this->Errors->addError(
        'STRING_IS_NOT_IN_ARRAY',
        ErrorCodes::STRING_IS_NOT_IN_ARRAY,
      );
    }

    return $this;
  }

  /**
   * Check if tested value is not in given array.
   *
   * @param array $expected Array to check.
   * @return $this
   */
  public function isNotInArray(array $expected): StringValidator {
    if (in_array($this->value, $expected, true)) {
      $this->Errors->addError(
        'STRING_IS_IN_ARRAY',
        ErrorCodes::STRING_IS_IN_ARRAY,
      );
    }

    return $this;
  }

}
