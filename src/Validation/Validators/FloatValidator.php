<?php
namespace DarkFox\Fur\Validation\Validators;

use DarkFox\Fur\Validation\ErrorCodes;

class FloatValidator extends BaseValidator
{
  /**
   * Check if tested float is greater than expected.
   *
   * @param int $expected Integer to check.
   * @return $this
   */
  public function isGreater(int $expected): FloatValidator {
    if ($expected >= $this->value) {
      $this->Errors->addError(
        'FLOAT_SHOULD_BE_GREATER',
        ErrorCodes::FLOAT_SHOULD_BE_GREATER,
      );
    }

    return $this;
  }

  /**
   * Check if tested float is greater or equal than expected.
   *
   * @param int $expected Integer to check.
   * @return $this
   */
  public function isGreaterOrEqual(int $expected): FloatValidator {
    if ($expected > $this->value) {
      $this->Errors->addError(
        'FLOAT_SHOULD_BE_GREATER_OR_EQUAL',
        ErrorCodes::FLOAT_SHOULD_BE_GREATER_OR_EQUAL,
      );
    }

    return $this;
  }

  /**
   * Check if tested float is lower than expected.
   *
   * @param int $expected Integer to check.
   * @return $this
   */
  public function isLower(int $expected): FloatValidator {
    if ($expected <= $this->value) {
      $this->Errors->addError(
        'FLOAT_SHOULD_BE_LOWER',
        ErrorCodes::FLOAT_SHOULD_BE_LOWER,
      );
    }

    return $this;
  }

  /**
   * Check if tested float is lower or equal than expected.
   *
   * @param int $expected Integer to check.
   * @return $this
   */
  public function isLowerOrEqual(int $expected): FloatValidator {
    if ($expected < $this->value) {
      $this->Errors->addError(
        'FLOAT_SHOULD_BE_LOWER_OR_EQUAL',
        ErrorCodes::FLOAT_SHOULD_BE_LOWER_OR_EQUAL,
      );
    }

    return $this;
  }

  /**
   * Check if tested float is divided by given value.
   *
   * @param int|float $divider Divider to check.
   * @return $this
   */
  public function isDividedBy($divider): FloatValidator {
    if (!is_int($divider) && !is_float($divider)) {
      $this->Errors->addError(
        'DIVIDER_SHOULD_BE_INTEGER_OR_FLOAT',
        ErrorCodes::DIVIDER_SHOULD_BE_INTEGER_OR_FLOAT,
      );
    }

    if (0 !== $this->value % $divider) {
      $this->Errors->addError(
        'FLOAT_IS_NOT_DIVIDED',
        ErrorCodes::FLOAT_IS_NOT_DIVIDED,
      );
    }

    return $this;
  }

  /**
   * Check if tested float is not divided by given value.
   *
   * @param int|float $divider Divider to check.
   * @return $this
   */
  public function isNotDividedBy($divider): FloatValidator {
    if (!is_int($divider) && !is_float($divider)) {
      $this->Errors->addError(
        'DIVIDER_SHOULD_BE_INTEGER_OR_FLOAT',
        ErrorCodes::DIVIDER_SHOULD_BE_INTEGER_OR_FLOAT,
      );
    }

    if (0 === $this->value % $divider) {
      $this->Errors->addError(
        'FLOAT_IS_DIVIDED',
        ErrorCodes::FLOAT_IS_DIVIDED,
      );
    }

    return $this;
  }

}
