<?php
namespace DarkFox\Fur\Validation\Validators;

use DarkFox\Fur\Validation\ErrorCodes;

class ArrayValidator extends BaseValidator
{
  /**
   * Check if tested value is in array.
   *
   * @param mixed $expected Value to check.
   * @return $this
   */
  public function contains($expected): ArrayValidator {
    if (!in_array($expected, $this->value, true)) {
      $this->Errors->addError(
        'ARRAY_DOES_NOT_CONTAIN',
        ErrorCodes::ARRAY_DOES_NOT_CONTAIN,
      );
    }

    return $this;
  }

  /**
   * Check if tested value is not in array.
   *
   * @param mixed $expected Value to check.
   * @return $this
   */
  public function notContains($expected): ArrayValidator {
    if (in_array($expected, $this->value, true)) {
      $this->Errors->addError(
        'ARRAY_DOES_CONTAIN',
        ErrorCodes::ARRAY_DOES_CONTAIN,
      );
    }

    return $this;
  }

  /**
   * Check if tested value exists as an array key.
   *
   * @param mixed $expected Value to check.
   * @return $this
   */
  public function hasKey($expected): ArrayValidator {
    if (!array_key_exists($expected, $this->value)) {
      $this->Errors->addError(
        'ARRAY_KEY_NOT_EXISTS',
        ErrorCodes::ARRAY_KEY_NOT_EXISTS,
      );
    }

    return $this;
  }

  /**
   * Check if tested value is in array.
   *
   * @param mixed $expected Value to check.
   * @return $this
   */
  public function dontHaveKey($expected): ArrayValidator {
    if (array_key_exists($expected, $this->value)) {
      $this->Errors->addError(
        'ARRAY_KEY_EXISTS',
        ErrorCodes::ARRAY_KEY_EXISTS,
      );
    }

    return $this;
  }

  /**
   * Check if tested array size is greater than expected.
   *
   * @param int $expected Integer got check.
   * @return $this
   */
  public function isSizeGreaterThan(int $expected): ArrayValidator {
    if ($expected >= count($this->value)) {
      $this->Errors->addError(
        'ARRAY_SIZE_SHOULD_BE_GREATER',
        ErrorCodes::ARRAY_SIZE_SHOULD_BE_GREATER,
      );
    }

    return $this;
  }

  /**
   * Check if tested array size is greater or equal than expected.
   *
   * @param int $expected Integer got check.
   * @return $this
   */
  public function isSizeGreaterOrEqualThan(int $expected): ArrayValidator {
    if ($expected > count($this->value)) {
      $this->Errors->addError(
        'ARRAY_SIZE_SHOULD_BE_GREATER_OR_EQUAL',
        ErrorCodes::ARRAY_SIZE_SHOULD_BE_GREATER_OR_EQUAL,
      );
    }

    return $this;
  }

  /**
   * Check if tested array size is greater than expected.
   *
   * @param int $expected Integer got check.
   * @return $this
   */
  public function isSizeLowerThan(int $expected): ArrayValidator {
    if ($expected <= count($this->value)) {
      $this->Errors->addError(
        'ARRAY_SIZE_SHOULD_BE_LOWER',
        ErrorCodes::ARRAY_SIZE_SHOULD_BE_LOWER,
      );
    }

    return $this;
  }

  /**
   * Check if tested array size is greater or equal than expected.
   *
   * @param int $expected Integer got check.
   * @return $this
   */
  public function isSizeLowerOrEqualThan(int $expected): ArrayValidator {
    if ($expected < count($this->value)) {
      $this->Errors->addError(
        'ARRAY_SIZE_SHOULD_BE_LOWER_OR_EQUAL',
        ErrorCodes::ARRAY_SIZE_SHOULD_BE_LOWER_OR_EQUAL,
      );
    }

    return $this;
  }

}
