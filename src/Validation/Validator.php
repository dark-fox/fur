<?php
namespace DarkFox\Fur\Validation;

class Validator
{
  protected Rules $Rules;
  protected Errors $Errors;

  public function __construct() {
    $this->Errors = new Errors;
  }

  /**
   * Set value field with data to check with further functions.
   *
   * @param mixed $value Value to test.
   * @return Rules
   */
  public function check($value): Rules {
    $this->Rules = new Rules($value, $this->Errors);
    return $this->Rules;
  }

  /**
   * Returns true if an error array is empty.
   *
   * @return bool
   */
  public function noError(): bool {
    return $this->Errors->noErrors();
  }

  /**
   * Returns true if an error array is filled with errors.
   *
   * @return bool
   */
  public function hasErrors(): bool {
    return $this->Errors->hasErrors();
  }

  /**
   * Return arrays with errors.
   *
   * @return array
   */
  public function getErrors(): array {
    return $this->Errors->getErrors();
  }

}
