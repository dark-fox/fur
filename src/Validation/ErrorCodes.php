<?php
namespace DarkFox\Fur\Validation;

class ErrorCodes {
  //Base validation
  public const VALUES_ARE_NOT_EQUAL = 100;
  public const VALUES_ARE_EQUAL = 101;
  public const DIVIDER_SHOULD_BE_INTEGER_OR_FLOAT = 102;

  //String validation
  public const VALUE_IS_NOT_A_STRING = 200;
  public const WRONG_LENGTH = 201;
  public const STRING_SHOULD_BE_LONGER = 202;
  public const STRING_SHOULD_BE_LONGER_OR_EQUAL = 203;
  public const STRING_SHOULD_BE_SHORTER = 204;
  public const STRING_SHOULD_BE_SHORTER_OR_EQUAL = 205;
  public const STRING_DOES_NOT_CONTAIN = 206;
  public const STRING_DOES_CONTAIN = 207;
  public const STRING_IS_NOT_AN_EMAIL = 208;
  public const STRING_IS_NOT_A_URL = 209;
  public const STRING_IS_NOT_IN_ARRAY = 210;
  public const STRING_IS_IN_ARRAY = 211;

  //Integer validation
  public const VALUE_IS_NOT_AN_INTEGER = 300;
  public const INTEGER_SHOULD_BE_GREATER = 301;
  public const INTEGER_SHOULD_BE_GREATER_OR_EQUAL = 302;
  public const INTEGER_SHOULD_BE_LOWER = 303;
  public const INTEGER_SHOULD_BE_LOWER_OR_EQUAL = 304;
  public const INTEGER_IS_NOT_DIVIDED = 305;
  public const INTEGER_IS_DIVIDED = 306;
  public const INTEGER_IS_NOT_EVEN = 307;
  public const INTEGER_IS_NOT_ODD = 308;

  //Float validation
  public const VALUE_IS_NOT_A_FLOAT = 400;
  public const FLOAT_SHOULD_BE_GREATER = 301;
  public const FLOAT_SHOULD_BE_GREATER_OR_EQUAL = 302;
  public const FLOAT_SHOULD_BE_LOWER = 303;
  public const FLOAT_SHOULD_BE_LOWER_OR_EQUAL = 304;
  public const FLOAT_IS_NOT_DIVIDED = 305;
  public const FLOAT_IS_DIVIDED = 306;

  //Boolean validation
  public const VALUE_IS_NOT_A_BOOLEAN = 500;
  public const BOOLEAN_IS_NOT_TRUE = 501;
  public const BOOLEAN_IS_NOT_FALSE = 502;

  //Array validation
  public const VALUE_IS_NOT_AN_ARRAY = 600;
  public const ARRAY_DOES_NOT_CONTAIN = 601;
  public const ARRAY_DOES_CONTAIN = 602;
  public const ARRAY_KEY_NOT_EXISTS = 603;
  public const ARRAY_KEY_EXISTS = 604;
  public const ARRAY_SIZE_SHOULD_BE_GREATER = 605;
  public const ARRAY_SIZE_SHOULD_BE_GREATER_OR_EQUAL = 606;
  public const ARRAY_SIZE_SHOULD_BE_LOWER = 607;
  public const ARRAY_SIZE_SHOULD_BE_LOWER_OR_EQUAL = 608;

}
