<?php
namespace DarkFox\Fur\Validation;

use DarkFox\Fur\Validation\Validators\ArrayValidator;
use DarkFox\Fur\Validation\Validators\BooleanValidator;
use DarkFox\Fur\Validation\Validators\FloatValidator;
use DarkFox\Fur\Validation\Validators\IntegerValidator;
use DarkFox\Fur\Validation\Validators\StringValidator;

class Rules
{
  protected Errors $Errors;
  protected $value;

  /**
   * Rules constructor.
   *
   * @param mixed  $value  Value to test.
   * @param Errors $Errors Validation errors handler instance.
   */
  public function __construct($value, Errors $Errors) {
    $this->Errors = $Errors;
    $this->value = $value;
  }

  /**
   * Check if tested value is a string.
   *
   * @return StringValidator
   */
  public function isString(): StringValidator {
    if (!is_string($this->value)) {
      $this->Errors->addError(
        'VALUE_IS_NOT_A_STRING',
        ErrorCodes::VALUE_IS_NOT_A_STRING,
      );
    }

    return new StringValidator($this->value, $this->Errors);
  }

  /**
   * Check if tested value is an integer.
   *
   * @return IntegerValidator
   */
  public function isInteger(): IntegerValidator {
    if (!is_int($this->value)) {
      $this->Errors->addError(
        'VALUE_IS_NOT_AN_INTEGER',
        ErrorCodes::VALUE_IS_NOT_AN_INTEGER,
      );
    }

    return new IntegerValidator($this->value, $this->Errors);
  }

  /**
   * Check if tested value is a float.
   *
   * @return FloatValidator
   */
  public function isFloat(): FloatValidator {
    if (!is_float($this->value)) {
      $this->Errors->addError(
        'VALUE_IS_NOT_A_FLOAT',
        ErrorCodes::VALUE_IS_NOT_A_FLOAT,
      );
    }

    return new FloatValidator($this->value, $this->Errors);
  }

  /**
   * Check if tested value is a boolean.
   *
   * @return BooleanValidator
   */
  public function isBool(): BooleanValidator {
    if (!is_bool($this->value)) {
      $this->Errors->addError(
        'VALUE_IS_NOT_A_BOOLEAN',
        ErrorCodes::VALUE_IS_NOT_A_BOOLEAN,
      );
    }

    return new BooleanValidator($this->value, $this->Errors);
  }

  /**
   * Check if tested value is an array.
   *
   * @return ArrayValidator
   */
  public function isArray(): ArrayValidator {
    if (!is_array($this->value)) {
      $this->Errors->addError(
        'VALUE_IS_NOT_AN_ARRAY',
        ErrorCodes::VALUE_IS_NOT_AN_ARRAY,
      );
    }

    return new ArrayValidator($this->value, $this->Errors);
  }

}
