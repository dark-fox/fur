<?php
namespace DarkFox\Fur\Validation;

use Exception;

class Errors
{
  protected array $errors = [];

  /**
   * Returns true if an error array is filled with errors.
   *
   * @return bool
   */
  public function hasErrors(): bool {
    return 0 < count($this->errors);
  }

  /**
   * Returns true if an error array is empty.
   *
   * @return bool
   */
  public function noErrors(): bool {
    return 0 === count($this->errors);
  }

  /**
   * Store new error in errors array.
   *
   * @param string $message Error message.
   * @param int    $code    Optional (by default set to 0) error code.
   * @return Errors
   */
  public function addError(string $message, int $code = 0): Errors {
    $error = [
      'code' => $code,
      'message' => $message,
    ];

    $Exception = new Exception;

    if (isset($Exception->getTrace()[1])) {
      $caller = $Exception->getTrace()[1];
      $error['caller'] = [
        'class' => $caller['class'] ?? '',
        'function' => $caller['function'] ?? '',
        'line' => $caller['line'] ?? '',
      ];
    }

    $this->errors[] = $error;

    return $this;
  }

  /**
   * Return arrays with errors.
   *
   * @return array
   */
  public function getErrors(): array {
    return $this->errors;
  }

}
