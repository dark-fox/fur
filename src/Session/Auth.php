<?php
namespace DarkFox\Fur\Session;

class Auth extends EncryptedSession
{
  public const FIELD_ID = 'id';
  public const FIELD_EMAIL = 'email';

  protected $Config = AuthConfig::class;

  /**
   * Create authentication session.
   *
   * @param int    $userId    User id to store in session.
   * @param string $userEmail E-Mail to store in session.
   * @return bool
   */
  public function login(int $userId, string $userEmail): bool {
    if (!$this->isLogged()) {
      $sessionData = [
        $this->encrypt(static::FIELD_ID) => $this->encrypt($userId),
        $this->encrypt(static::FIELD_EMAIL) => $this->encrypt($userEmail),
      ];
      $sessionData = json_encode($sessionData);
      Session::getInstance()->set($this->sessionName, $sessionData);
    }

    return true;
  }

  /**
   * Remove authentication session data.
   */
  public function logout(): void {
    Session::getInstance()->remove($this->sessionName);
  }

  /**
   * Return true if user is logged (authentication session was made).
   *
   * @return bool
   */
  public function isLogged(): bool {
    return count($this->sessionData) > 0;
  }

}
