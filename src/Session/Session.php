<?php
namespace DarkFox\Fur\Session;

use DarkFox\Fur\Data\Exceptions\ValuesException;
use DarkFox\Fur\Data\Request;
use DarkFox\Fur\Tools\Singleton;

class Session extends Singleton
{
  /**
   * Get data from $_SESSION.
   *
   * @param string $field Field to get.
   * @return string|null
   */
  public function get(string $field): ?string {
    try {
      $content = (new Request)->session($field)->string();
    } catch (ValuesException $exception) {
      $content = '';
    }

    return $content;
  }

  /**
   * Store data in $_SESSION.
   *
   * @param string     $field Field to set.
   * @param null|mixed $value Value of the field.
   */
  public function set(string $field, $value = null): void {
    if (is_string($value) || is_numeric($value)) {
      $_SESSION[$field] = $value;
    }
  }

  /**
   * Remove data from $_SESSION.
   *
   * @param string $field Field to remove.
   */
  public function remove(string $field): void {
    if (!is_null($this->get($field))) {
      unset($_SESSION[$field]);
    }
  }

}
