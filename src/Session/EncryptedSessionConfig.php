<?php
namespace DarkFox\Fur\Session;

use DarkFox\Fur\Config\Config;

class EncryptedSessionConfig extends Config
{
  public string $sessionName;
  public string $cipher;
  public string $key;
  public string $iv;

}
