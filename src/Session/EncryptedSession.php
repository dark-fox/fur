<?php
namespace DarkFox\Fur\Session;

use DarkFox\Fur\Data\Exceptions\ValuesException;
use DarkFox\Fur\Data\Request;
use DarkFox\Fur\Tools\Singleton;

class EncryptedSession extends Singleton
{
  protected string $sessionName;
  protected array $sessionData = [];
  /** @var EncryptedSessionConfig */
  protected $Config;

  /**
   * EncryptedSession constructor.
   */
  public function __construct() {
    if (is_string($this->Config) && class_exists($this->Config)) {
      $this->Config = $this->Config::getInstance();
    }

    $this->sessionName = $this->encrypt($this->Config->sessionName);
    $this->getSessionData();
  }

  /**
   * Get data from $_SESSION.
   *
   * @param string $field Field to get.
   * @return string
   */
  public function get(string $field): string {
    $decryptedContent = '';
    $encryptedKey = $this->encrypt($field);

    if (isset($this->sessionData[$encryptedKey])) {
      $decryptedContent = $this->decrypt($this->sessionData[$encryptedKey]);
    }

    return $decryptedContent;
  }

  /**
   * Store data in $_SESSION.
   *
   * @param string $field Name of the field to set.
   * @param string $value Value to set.
   */
  public function set(string $field, string $value): void {
    $this->sessionData[$this->encrypt($field)] = $this->encrypt($value);
    Session::getInstance()->set($this->sessionName, $this->sessionData);
  }

  /**
   * Encrypts $_SESSION data.
   *
   * @param string $content Content to encrypt.
   * @return string
   */
  protected function encrypt(string $content): string {
    $encryptedContent = openssl_encrypt(
      $content,
      $this->Config->cipher,
      $this->Config->key,
      0,
      $this->Config->iv
    );

    if (false === $encryptedContent) {
      $encryptedContent = '';
    }

    return $encryptedContent;
  }

  /**
   * Decrypts $_SESSION data.
   *
   * @param string $content Content to decrypt.
   * @return string
   */
  protected function decrypt(string $content): string {
    $decryptedContent = openssl_decrypt(
      $content,
      $this->Config->cipher,
      $this->Config->key,
      0,
      $this->Config->iv
    );

    if (false === $decryptedContent) {
      $decryptedContent = '';
    }

    return $decryptedContent;
  }

  /**
   * Set data from $_SESSION to sessionData property,.
   */
  protected function getSessionData(): void {
    try {
      $this->sessionData = (new Request)->session($this->sessionName)->json();
    } catch (ValuesException $exception) {
      $this->sessionData = [];
    }
  }

}
